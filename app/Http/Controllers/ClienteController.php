<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Cliente;

class ClienteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function InicioCliente()
    {
        //dd('Hola Mundillo');
        $Cliente = Cliente::all();
        //dd($Cliente);
        return view('Clientes.inicio')->with('Cliente', $Cliente);
    }

    public function RegistrarCliente(Request $request)
    {
        $Cliente = Cliente::all();
        return view('Clientes.registrar')->with('Cliente', $Cliente);
    }

    public function GuardarCliente(Request $request){

        //dd($request);

        $this->validate($request, [

            'Nombres' => 'required',
            'Apellidos' => 'required',
            'Cedula' => 'required',
            'Direccion' => 'required',
            'Telefono' => 'required',
            'Fecha_de_Nacimiento' => 'required',
            'Email' => 'required'
        ]);

        $Cliente = new Cliente;
            $Cliente->Nombres             = $request->Nombres;
            $Cliente->Apellidos           = $request->Apellidos;
            $Cliente->Cedula              = $request->Cedula;
            $Cliente->Direccion           = $request->Direccion;
            $Cliente->Telefono            = $request->Telefono;
            $Cliente->Fecha_de_Nacimiento = $request->Fecha_de_Nacimiento;
            $Cliente->Email               = $request->Email;

            $Cliente->save();

            return redirect()->route('data.clientes');

    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
