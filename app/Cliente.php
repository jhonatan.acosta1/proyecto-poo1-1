<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cliente extends Model
{
    protected $fillable = [
        'Nombres',
        'Apellidos',
        'Cedula',
        'Direccion',
        'Telefono',
        'Fecha_de_Nacimiento',
        'Email',
    ];
}
