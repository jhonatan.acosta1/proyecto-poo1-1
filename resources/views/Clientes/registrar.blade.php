@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Registrar Nuevo Cliente</div>

                <div class= "col text-right">
                    <a href="{{ route('data.clientes') }}" class="btn btn-sm btn-success">Cancelar Registro</a>
                </div>

                <div class="card-body">

                    <form role="form" method="post" action="{{ route('guardar.clientes')}}">
                        {{ csrf_field() }}
                        {{ method_field('post') }}

                        <div class="row">

                            <div class="col-lg-4">
                                <label class="from-control-label" for="Nombres">Nombre del Usuario</label>
                                <input type="text" class="from-control" name="Nombres">
                            </div>

                            <div class="col-lg-4">
                                <label class="from-control-label" for="Apellidos">Apellidos del Usuario</label>
                                <input type="text" class="from-control" name="Apellidos">
                            </div>

                            <div class="col-lg-4">
                                <label class="from-control-label" for="Cedula">Cedula</label>
                                <input type="number" class="from-control" name="Cedula">
                            </div>

                            <div class="col-lg-4">
                                <label class="from-control-label" for="Direccion">Dirección</label>
                                <input type="text" class="from-control" name="Direccion">
                            </div>

                            <div class="col-lg-4">
                                <label class="from-control-label" for="Telefono">Telefono</label>
                                <input type="number" class="from-control" name="Telefono">
                            </div>

                            <div class="col-lg-4">
                                <label class="from-control-label" for="Fecha_de_Nacimiento">Fecha de Nacimiento</label>
                                <input type="number" class="from-control" name="Fecha_de_Nacimiento">
                            </div>

                            <div class="col-lg-4">
                                <label class="from-control-label" for="Email">Email</label>
                                <input type="text" class="from-control" name="Email">
                            </div>

                        </div>

                        <button type="submit" class="btn btn-success pull-right"> Crear Usuario </button>
                    </form>
               
            </div>
        </div>
    </div>
</div>
@endsection