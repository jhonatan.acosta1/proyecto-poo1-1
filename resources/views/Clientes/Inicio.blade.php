@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Listas de Clientes</div>

                <div class="col text-right">

                    <a href="{{ route('registrar.clientes') }}" class="btn btn-sm btn-primary">Nuevo Cliente</a>
                
                </div>

                <div class="card-body">

                <table class="table">
        <thead>
            <tr>
                <th scope="col">#ID</th>
                <th scope="col">Nombres</th>
                <th scope="col">Apellidos</th>
                <th scope="col">Cedula</th>
                <th scope="col">Dirección</th>
                <th scope="col">Telefono</th>
                <th scope="col">Fecha De Nacimiento</th>
                <th scope="col">Email</th>
            </tr>
        </thead>
        <tbody>

            @foreach ($Cliente as $item)
                <tr>
                <th scope="row">{{$item->id}}</th>
                <td>{{$item->Nombres}}</td>
                <td>{{$item->Apellidos}}</td>
                <td>{{$item->Cedula}}</td>
                <td>{{$item->Direccion}}</td>
                <td>{{$item->Telefono}}</td>
                <td>{{$item->Fecha_de_Nacimiento}}</td>
                <td>{{$item->Email}}</td>
                </tr>
            @endforeach

        </tbody>
    </table>    

                </div>
            </div>
        </div>
    </div>
</div>
@endsection